/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

/**
 *
 * @author Hiero
 */
public class EntryAnswer extends Event{
    ArrayList<Connection> list;
    
    public EntryAnswer(ArrayList<Connection> list) {
        super();
        this.type = 2;
        this.list = list;
    }

    public EntryAnswer() {
        super();
        this.type = 2;
        this.list = new ArrayList<>();
    }

    @Override
    public String toJSON() {
        Map jsonDict = new LinkedHashMap();
        JSONArray array = new JSONArray();
        for(int i = 0; i < list.size(); i++){
            try {
                array.add(list.get(i).toJSON());
            } catch (IOException ex) {
                Logger.getLogger(EntryAnswer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        jsonDict.put("list", array);
        jsonDict.put("type", 2);
        String json = JSONValue.toJSONString(jsonDict);
        return json;
    }

    @Override
    public void fromJSON(String json) {
        list.clear();
        Map m = (Map) JSONValue.parse(json);
        JSONArray array = new JSONArray();
        array = (JSONArray) m.get("list");
        for(int i = 0; i < array.size(); i++){
            Connection connection = new Connection();
            connection.fromJSON(array.get(i).toString());
            list.add(connection);
        }
    }
    
}
