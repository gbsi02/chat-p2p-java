/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONValue;

/**
 *
 * @author Hiero
 */
public class EntryEvent extends Event{
    Connection connect;
    
    public EntryEvent(Connection connect) {
        super();
        this.type = 1;
        this.connect = connect;
    }

    public EntryEvent() {
        super();
        this.type = 1;
    }

    @Override
    public String toJSON() {
        try {
            Map jsonDict = new LinkedHashMap();
            jsonDict.put("connection", connect.toJSON());
            jsonDict.put("type", 1);
            String json = JSONValue.toJSONString(jsonDict);
            return json;
        } catch (IOException ex) {
            Logger.getLogger(EntryEvent.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void fromJSON(String json) {
        Map m = (Map) JSONValue.parse(json);
        connect = new Connection();
        connect.fromJSON((String) m.get("connection"));
    }
    
    
}
