/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.simple.JSONValue;

/**
 *
 * @author Hiero
 */
public class Connection {
    public String address;
    public long port;
    
    public Connection(){

    }
    
    public Connection(String address, int port){
        this.address = address;
        this.port = port;
    }
    
    public String toJSON() throws IOException{
        Map jsonDict = new LinkedHashMap();
        jsonDict.put("addr", address);
        jsonDict.put("port", port);
        String json = JSONValue.toJSONString(jsonDict);
        return json;
    }
    
    public void fromJSON(String json){
        Map m = (Map) JSONValue.parse(json);
        address = (String) m.get("addr");
        port = (long) m.get("port");
    }
}
