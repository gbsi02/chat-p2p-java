/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author Hiero
 */
public abstract class Event {
    int type;
    
    public Event(){
        this.type = 0;
    }
    
    public abstract String toJSON();
    
    public abstract void fromJSON(String json);
}
