/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hiero
 */
public class ListManager extends Thread {
    private boolean first;
    private Connection connect;
    private ArrayList<Connection> list;
    private ArrayList<Connection> newlist;
    private ArrayList<Connection> wantToLeave;
    private ArrayList<Connection> wantToEnter;
    boolean connected = false;
    
    public ListManager(boolean first) throws UnknownHostException{
        this.first = first;
        list = new ArrayList<>();
        wantToLeave = new ArrayList<>();
        connect = new Connection(InetAddress.getLocalHost().getHostAddress(), 6002);
    }
    
    @Override
    public void run(){
        try {
            Socket client = new Socket(connect.address, (int) connect.port);
            InputStreamReader in = new InputStreamReader(client.getInputStream());
            BufferedReader c = new BufferedReader(in);
            String message = c.readLine();
            Event e = EventListener.getEvent(message);
            if(e.type == 2){
                EntryAnswer event = (EntryAnswer) e;
                newlist = event.list;
                for(int i = 0; i < list.size(); i++){
                    for(int j = 0; j < newlist.size(); j++){
                        if(!list.get(i).equals(newlist.get(j))){
                            if(!list.contains(newlist.get(j))){
                                if(wantToEnter.contains(newlist.get(j))){
                                    list.add(newlist.get(j));
                                    wantToEnter.remove(newlist.get(j));
                                }
                            }
                        }else if(!newlist.get(i).equals(list.get(i))){
                            if(wantToLeave.contains(newlist.get(i))){
                                list.remove(i);
                                wantToLeave.remove(list.get(i));
                            }else{
                                //TODO: Checa se o elemento esta vivo.
                            }
                        }
                        newlist.remove(j);
                    }
                }
            }
            //TODO: Enviar a lista para o proximo no.
        } catch (IOException ex) {
            Logger.getLogger(ListManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void createList(Connection connect){
        list.add(this.connect);
        list.add(connect);
    }
    
}
