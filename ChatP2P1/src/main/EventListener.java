/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONValue;

/**
 *
 * @author Hiero
 */
public class EventListener extends Thread{
    ServerSocket s;
    Socket s1;
    
    public EventListener(int port) throws IOException{
        s = new ServerSocket(port);
        s1 = s.accept();
    }
    
    @Override
    public void run(){
        try {
            InputStreamReader in = new InputStreamReader(s1.getInputStream());
            BufferedReader b = new BufferedReader(in);
            String message = b.readLine();
            Event e = getEvent(message);
            if(e.type == 1){
                EntryEvent event = (EntryEvent) e;
                entryEventListener(event);
            }else if(e.type == 2){
                EntryAnswer event = (EntryAnswer) e;
                entryAnswerListener(event);
            }
        } catch (IOException ex) {
            Logger.getLogger(EventListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Event getEvent(String json){
        Map jsonDict = (Map) JSONValue.parse(json);
        if((long) jsonDict.get("type") == 1){
            EntryEvent e = new EntryEvent();
            e.fromJSON(json);
            return e;
        }else if((long) jsonDict.get("type") == 2){
            EntryAnswer e = new EntryAnswer();
            e.fromJSON(json);
            return e;
        }
        return null;
    }

    private void entryEventListener(EntryEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void entryAnswerListener(EntryAnswer event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

