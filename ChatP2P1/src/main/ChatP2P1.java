/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Hiero
 */
public class ChatP2P1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException{
        boolean first = false;
        String ip_to_connect;
        ServerSocket s;
        Socket client;
        InputStreamReader in;
        int EVENT_PORT = 6001;
        Connection connect = new Connection(InetAddress.getLocalHost().getHostAddress(), EVENT_PORT);
        
        if(args[0].equalsIgnoreCase("first")){
            first = true;
        }else{
            if(args.length < 1){
                System.out.println("You must pass either first or connect and an IP. Example:\n <myprogram>.jar first \n or \n <myprogram>.jar connect <nodeip>\n");
                return;
            }
        }
        ip_to_connect = args[1];
        Messenger messenger = new Messenger();
        s = new ServerSocket(EVENT_PORT);
        if(first){
            ListManager listManager = new ListManager(first);
            
            Socket s1 = s.accept();
            in = new InputStreamReader(s1.getInputStream());
            BufferedReader b = new BufferedReader(in);
            String message = b.readLine();
            s1.close();
            s.close();
            
            Event e = EventListener.getEvent(message);
            EntryEvent event;
            if(e.type != 1){
                System.out.println("Evento errado encontrado.");
                System.exit(0);
            }
            event = (EntryEvent) e;
            System.out.println("Evento de entrada do host " + event.connect.address + ".");
            listManager.createList(event.connect);
            listManager.start();
        }else{
            EntryEvent event = new EntryEvent(connect);
            client = new Socket(ip_to_connect, EVENT_PORT);
            OutputStreamWriter out = new OutputStreamWriter(client.getOutputStream());
            BufferedWriter b = new BufferedWriter(out);
            b.write(event.toJSON());
            
            in = new InputStreamReader(client.getInputStream());
            BufferedReader c = new BufferedReader(in);
            String message = c.readLine();
            EntryAnswer event1 = (EntryAnswer) EventListener.getEvent(message);
            ArrayList<Connection> toSend = event1.list;
            toSend.removeIf(i -> !i.address.equals(connect.address));
            toSend.removeIf(i -> !i.address.equals(ip_to_connect));
            ListManager listManager = new ListManager(first);
            listManager.start();
            while(!listManager.connected && toSend.size() > 0){
                Random random = new Random();
                int pos = random.nextInt(toSend.size());
                Connection element = toSend.get(pos);
                toSend.remove(pos);
                
                client = new Socket(element.address, EVENT_PORT);
                OutputStreamWriter out1 = new OutputStreamWriter(client.getOutputStream());
                BufferedWriter b1 = new BufferedWriter(out);
                b.write(event.toJSON());
                Thread.sleep(200);
            }
            while(!listManager.connected){
                Thread.sleep(200);
            }
        }
        messenger.start();
        EventListener eventListener = new EventListener(EVENT_PORT);
        eventListener.start();
        
        
        
        
        
        
        
//        String json = "{\"connection\": \"{\\\"addr\\\": \\\"addr\\\", \\\"port\\\": 1234}\", \"type\": 1}";
//        String json1 = "{\"connection\": \"{\\\"addr\\\": \\\"addr\\\", \\\"port\\\": 1234}\", \"type\": 1}";
        
//        ArrayList<Connection> l = new ArrayList<>();
//        Connection c = new Connection("0.0.0.0", 123);
//        Connection d = new Connection("0.0.0.1", 124);
//        l.add(c);
//        l.add(d);
//        EntryAnswer e = new EntryAnswer(l);
//        EntryAnswer f = new EntryAnswer(new ArrayList<>());
//        f.fromJSON(e.toJSON());
//        System.out.println(f.list.get(0).address);
//        System.out.println(f.list.get(1).port);
        
//        EntryEvent ee = new EntryEvent(c);
//        ee.fromJSON("{\"connection\": \"{\\\"addr\\\": \\\"addr\\\", \\\"port\\\": 1234}\", \"type\": 1}");
//        System.out.println(ee.connect.address + "\n" + ee.connect.port);
    }

}
