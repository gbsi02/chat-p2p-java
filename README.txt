﻿Arquivo jar não compilado.
O projeto pode ser aberto no Netbeans e compilado, porém a única função implementada e testada é a conversão de mensagens de e para JSON.

1. Troca de mensagens em broadcast; (Não implementado)
2. Utilizar bibliotecas para trocas de mensagens:
	As classes connection e message possuem um método toJSON() e fromJSON(). Esses métodos realizam a conversão dos objetos para JSON e de JSON.
2. Quando um usuário entrar ou sair do chat, todos os integrantes da rede deverão ser notificados; (Não implementado)
3. Os usuários poderão trocar mensagens privadas; (Não implementado)
4. Os usuários deverão ser notificados quando uma mensagem não for entregue; (Não implementado)
5. A aplicação deverá ser capaz de realizar a troca de mensagens independentemente da linguagem utilizada na aplicação (Ou seja aplicações em Python deverão se comunicar com as aplicações desenvolvidas em Java e vice-versa); (Não implementado)
6. Utilizar um servidor de log para registrar as ações realizadas (Não implementado)
7. Deverá ser possível exibir todos os usuários que estão conectados a aplicação em um determinado momento; (Não implementado)