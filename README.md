#Especificação e Entrega do Projeto

##Objetivos do Projeto

O objetivo do projeto do curso é proporcionar profundidade em tópicos relacionados à disciplina de Redes de Computadores.

##Visão Geral

O projeto que será solicitado visa complementar o conteúdo abordado na disciplina. 

Este projeto deve ser feito em um grupo com no máximo 2 alunos. **IMPORTANTE: Entre em contato com o docente antes do primeiro checkpoint do projeto se você deseja mudar de grupo ou ter um número diferente de alunos. Após o primeiro checkpoint, mudanças de grupo não serão permitidas**

Os produtos específicos para o projeto ser considerado completo incluem **códigos fontes e uma monografia com no máximo 14 páginas no formato da SBC** (a ser disponibilizado no moodle), contendo: 

###Resumo

+ Agradecimentos (se for o caso)
+ Introdução e descrição do Problema
+ Fundamentação Teórica
+ Detalhamento da soluçao
+ Metodologia, ambientes e ferramentas Utilizadas
+ Modelagem do sistema a ser desenvolvido/estudado
+ Conclusões
+ Referências Bibliográficas

##Proposta do Projeto

A proposta do projeto deve conter as seguintes informações:

+ Os nomes dos membros da equipe e N. USP
+ Tema e Título do Projeto
+ Descrição das atividades que serão realizadas como parte do projeto.
+ Resultados esperados
+ Cronograma das atividades
+ Equipamentos e recursos necessários

##Checkpoints do Projeto

O plano de projeto deve ter entre uma e duas páginas e serem entregues via moodle, bem como a apresentação (um dia antes do Primeiro  e Segundo Checkpoint) por um dos membros eleitos pelo grupo e também em formato impresso no dia do Primeiro e Segundo Checkpoint. Todos os alunos devem contribuir no momento da apresentação do seu grupo.

Haverá dois checkpoints do projeto. Os pontos de verificação serão apresentações em PPT/PDF que cobrem o seguinte:

###Primeiro Checkpoint (**28/04/15**)

*Plano de Projeto:*

+ Lista aproximada dos pontos principais do projeto (o que vocês efetivamente vão abordar)
+ As responsabilidades dos membros da equipe
+ A informação da equipe
+ O progresso até o momento: o que foi concluído
+ Todos os resultados até o momento
+ Problemas encontrados

###Segundo Checkpoint (**19/05/15**)

+ Lista das atividades efetivamente cumpridas a partir do Primeiro Checkpoint com base no cronograma de atividades
+ Justificar possíveis alterações no plano de projeto após o Primeiro Checkpoint
+ Discussão dos resultados parciais

##Entrega

+ Entrega da Monografia e Códigos
+ A monografia deve ser entregue em um formato padrão que está disponível para download no moodle.
+ Os códigos também devem ser enviados diretamente no bitbucket (https://bitbucket.org/)
+ A monografia deve ter no máximo 14 páginas e ser enviada para o Moodle
+ A data da entrega será dia **23/06/15** via moodle por um dos membros eleitos pelo grupo.

##Tema do Projeto

O tema do projeto se resume em desenvolvimento de um sistema P2P de troca de mensagens que permita a comunicação entre usuários envolvendo alguns conceitos da disciplina de Redes de Computadores.

###Desenvolvimento

O projeto deverá:

1. **Ser implementado nas Linguagens Java e Python;**

2. **Utilizar sockets e bibliotecas para troca de mensagens (JSON ou XML)**

3. **Abordar os conceitos de uma arquitetura P2P pura.**

###Características básicas

A aplicação a ser desenvolvida baseia-se em uma janela de bate papo (chat), na qual os usuários conectados na rede P2P poderão trocar mensagens em broadcast e unicast.

As principais funcionalidades desse chat serão:

1. Quando um usuário entrar ou sair do chat, todos os integrantes da rede deverão ser notificados;
2. Os usuários poderão trocar mensagens privadas;
3. Os usuários deverão ser notificados quando uma mensagem não for entregue;
4. A aplicação deverá ser capaz de realizar a troca de mensagens independentemente da linguagem utilizada na aplicação (Ou seja aplicações em Python deverão se comunicar com as aplicações desenvolvidas em Java e vice-versa);
5. Utilizar um servidor de log para registrar as ações realizadas
6. Deverá ser possível exibir todos os usuários que estão conectados a aplicação em um determinado momento;
7. Bônus: Utilizar aspectos de segurança (Chave Pública/ Chave Privada) para as mensagens privadas;

###Ambiente de desenvolvimento e execução

Será utilizado o cluster do LaSDPC. As informações de acesso para cada grupo, bem como, cadastro dos usuários estarão disponíveis no moodle.

##Critérios de avaliação

A nota final será dada pela seguinte equação: **NF = ((0,40\*a + 0.35\*b + 0.25\*c)\*d)**, em que:

a = Avaliação da funcionalidade em relação ao enunciado: 40%;

b = Avaliação do código fonte: 35%;

c = Avaliação da documentação: 25%;

d = Avaliação da apresentação: Nota individual aos membros do grupo com valor entre 0 e 1;

Cada grupo terá **15 minutos para apresentar**. Após a apresentação perguntas direcionadas aos membros do grupo permitiram avaliar o conhecimento do respectivo membro sobre o projeto desenvolvido, considerando aspectos de implementação e conceitos ligados a disciplina.

##Considerações Finais

1. O entendimento da proposta, decisões sobre implementação, investigação sobre os conceitos exigidos fazem parte da avaliação final.

2. Se não for enviado o código fonte e a devida documentação, o grupo automaticamente receberá a nota zero.

3. Casos de cópias de trabalhos ocasionarão nota zero para todos os trabalhos semelhantes. É importante destacar que caso isso ocorra, a reprovação na disciplina é uma consequência devido aos critérios de avaliação da disciplina.

4. Os grupos poderão ser entrevistados pelo professor e estagiário PAE individualmente, de acordo com a necessidade, em datas e horários a serem definidos.

#**Entrega Final**
#**23/06/15**
#**23:55H**